﻿using UnityEngine;
using System.Collections.Generic;

public enum STATUS
{
    READY, MOVE, ARRIVE, USE, 
};

public class HaltSkill : MonoBehaviour
{
    //게임오브젝트들 정의
    Camera m_camera;
    GameObject m_sphere;
    GameObject[] m_normals;

    float high = 6.0f; //구체가 공중에 뜨는 높이(바닥으로부터)
    float dist = 25.0f; //구체 거리(플레이어와 xz평면상으로 얼마나 떨어져있나??)
    float speed = 0.1f; //발사되는 속도

    float scale = 0.9f; //구체 스케일이 줄어드는 비율
    float radius = 6.0f; //적 끌어당기는 범위 r

    Vector3 DirectionTo;//플레이어-목적지까지 방향벡터
    Vector3 CameraPos; //플레이어 좌표
    Vector3 HaltPos; //목적지 좌표
    Vector3 MovingPos;//구체 좌표

    STATUS ST;

    Dictionary<int, Vector3> BearInfo = new Dictionary<int, Vector3>();
    
    // Start is called before the first frame update
    void Start()
    {
        ST=STATUS.READY;
        m_camera = GetComponent<Camera>();
        m_sphere = GameObject.FindGameObjectWithTag("HALT");
        m_normals = GameObject.FindGameObjectsWithTag("TEST");
    }

    //클래스 생성자 역할
    void SetDefault() 
    {
        BearInfo.Clear();
        //카메라의 foward 방향 벡터를 얻어온다.
        Vector3 campos = new Vector3(m_camera.transform.forward.x, m_camera.transform.forward.y, m_camera.transform.forward.z);
        //Debug.Log("cam dir: "+ campos);

        CameraPos = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, m_camera.transform.position.z);

        float xpos = campos.x * dist;//x가 -면 -10 +면 +10만큼 좌표값 더함
        float zpos = campos.z * dist; //z가 -면 -10

        //구체를 복제하고 카메라 위치에서 생성
        MovingPos = CameraPos;
        Instantiate(m_sphere, CameraPos, Quaternion.identity);

        //구체 마지노선 좌표와 플레이어부터 구체까지의 방향벡터 정의
        HaltPos = new Vector3 (CameraPos.x + xpos, high, CameraPos.z + zpos);
        DirectionTo = (HaltPos - CameraPos).normalized;

        Debug.Log("시작 좌표: " + CameraPos + " 목적지: " + HaltPos + " 방향벡터:" + DirectionTo);
        ST = STATUS.MOVE;

    }

    // Update is called once per frame
    void Update()
    {
        switch (ST)
        {
            case STATUS.READY:
                if (Input.GetMouseButtonDown(0))//마우스 누름
                    SetDefault();
                break;

            case STATUS.MOVE:
                float gap = Vector3.Distance(MovingPos, HaltPos);
                //마우스 눌러서 중간에 멈추게 하거나, 갭이 0.5이하
                if (Input.GetMouseButtonDown(0) || gap <= 0.5f)
                    ST = STATUS.ARRIVE;
                
                else
                {
                    MovingPos += DirectionTo * speed;
                    m_sphere.transform.position = MovingPos;
                    //Debug.Log("이동중... " + MovePos);
                }
                break;

            case STATUS.ARRIVE:
                GetBearsPos();
                break;

            case STATUS.USE:
                PullBears(BearInfo.Count);
                break;
        }

    }

    void GetBearsPos() //곰돌이찾기
    {
        for (int i = 0; i < m_normals.Length; i++)
        {
            Vector3 normalpos = m_normals[i].transform.position;
            Vector2 nmgdist = new Vector2(Mathf.Abs(MovingPos.x - normalpos.x), Mathf.Abs(MovingPos.z - normalpos.z));
            //Debug.Log("노멀곰돌이와의 xz거리: " + nmgdist.x + "," +  nmgdist.y);

            if (nmgdist.x <= radius || nmgdist.y <= radius)
            {
                Debug.Log("잡혔당: " + m_normals[i].transform.position);
                Vector3 DirtoSphere=(MovingPos - m_normals[i].transform.position).normalized;
                BearInfo.Add(i, DirtoSphere);
            }
                
        }

        ST = STATUS.USE;
    }

    void PullBears(int count)
    {
        int Arrivecount = 0;
        m_sphere.transform.localScale = m_sphere.transform.localScale * 0.99f;

        foreach (int index in BearInfo.Keys)
        {
            float gap = Vector3.Distance(m_normals[index].transform.position, MovingPos);

            if (gap < 0.05f)
                Arrivecount++;
               
            else
                m_normals[index].transform.position += BearInfo[index] * speed;
                
        }

        //모든 곰돌이들이 도착했을 때 체크 하나만 도착했을때면 ㄴ
        if (Arrivecount == count)
        {
            ST = STATUS.READY;
        }


    }

}
